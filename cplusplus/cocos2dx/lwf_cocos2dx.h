/*
 * Copyright (C) 2013 GREE, Inc.
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef LWF_COCOS2DX_H
#define LWF_COCOS2DX_H

#include "lwf_cocos2dx_node.h"
#include "lwf.h"

namespace LWFNS {
	enum Actions {
		NONE            = 0,
		SET_INTERACTIVE = 1 << 0,
		RUN_EXEC        = 1 << 1,
		FIT_WIDTH       = 1 << 2,
		FIT_HEIGHT      = 1 << 3,

		// usefull combinations
		WIDTH_FULL      = FIT_WIDTH | SET_INTERACTIVE | RUN_EXEC,
		WIDTH_NOEXEC    = FIT_WIDTH | SET_INTERACTIVE,
		WIDTH_PASSIVE   = FIT_WIDTH | RUN_EXEC,

		HEIGHT_FULL     = FIT_HEIGHT | SET_INTERACTIVE | RUN_EXEC,
		HEIGHT_NOEXEC   = FIT_HEIGHT | SET_INTERACTIVE,
		HEIGHT_PASSIVE  = FIT_HEIGHT | RUN_EXEC
	};
	
	void setupLwf(shared_ptr<LWF>& lwf, int actions);
	cocos2d::LWFNode* createLwfNode(char const* const url, int actions);
};

#endif
