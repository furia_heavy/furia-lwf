/*
 * Copyright (C) 2013 GREE, Inc.
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef LWF_COCOS2DX_RESOURCECACHE_H
#define LWF_COCOS2DX_RESOURCECACHE_H

#include "base/CCPlatformMacros.h"
#include "base/CCValue.h"
#include "lwf_type.h"


namespace LWFNS {
struct Data;
}

NS_CC_BEGIN


struct LWFTextRendererContext
{
	enum {
		BMFONT,
		SYSTEMFONT,
		TTF,
	};

	int type;
	std::string font;

	LWFTextRendererContext() {}
	LWFTextRendererContext(int t, std::string f) : type(t), font(f) {}
};

class LWFResourceCache
{
private:
	typedef LWFNS::map<LWFNS::string,
		LWFNS::pair<int, LWFNS::shared_ptr<LWFNS::Data> > > DataCache;
	typedef LWFNS::map<LWFNS::Data *, DataCache::iterator> DataCacheMap;
	typedef LWFNS::pair<int, LWFNS::vector<LWFNS::PreloadCallback> > DataCallbackList;
	typedef LWFNS::map<LWFNS::string, DataCallbackList> DataCallbackMap;
	typedef LWFNS::map<LWFNS::string, LWFNS::pair<int, ValueMap> > ParticleCache;
	typedef LWFNS::map<LWFNS::string, LWFTextRendererContext> TextRendererCache_t;

private:
	static LWFResourceCache *m_instance;

private:
	DataCache m_dataCache;
	DataCacheMap m_dataCacheMap;
	DataCallbackMap m_dataCallbackMap;
	ParticleCache m_particleCache;
	TextRendererCache_t m_textRendererCache;
	LWFNS::string m_fontPathPrefix;
	LWFNS::string m_particlePathPrefix;

public:
	static LWFResourceCache *sharedLWFResourceCache();

public:
	LWFResourceCache();
	~LWFResourceCache();

	LWFNS::shared_ptr<LWFNS::Data> loadLWFData(const LWFNS::string &path);
	void unloadLWFData(const LWFNS::shared_ptr<LWFNS::Data> &data);

	ValueMap loadParticle(const LWFNS::string &path, bool retain = true);
	void unloadParticle(const LWFNS::string &path);

	void unloadAll();
	void dump();

	const LWFNS::string &getFontPathPrefix() { return m_fontPathPrefix; }
	void setFontPathPrefix(const LWFNS::string path) { m_fontPathPrefix = path; }
	LWFTextRendererContext getTextRendererContext(const LWFNS::string &font);

	const LWFNS::string &getDefaultParticlePathPrefix()
		{return m_particlePathPrefix;}
	void setDefaultParticlePathPrefix(const LWFNS::string path)
		{m_particlePathPrefix = path;}

private:
	void unloadLWFDataInternal(const LWFNS::shared_ptr<LWFNS::Data> &data);
	LWFNS::shared_ptr<LWFNS::Data> loadLWFDataInternal(const LWFNS::string &path);
};

NS_CC_END

#endif
