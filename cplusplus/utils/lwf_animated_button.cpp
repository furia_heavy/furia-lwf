#include "lwf_animated_button.h"
#include "lwf_cocos2dx.h"

namespace LWFNS {

AnimatedButton::AnimatedButton(Movie* root, char const* const buttonName, char const* const animationName) : movie(root) {
	button = movie->SearchButtonInstance(buttonName);
	pressEventHandler = button->AddEventHandler("press", std::bind(&AnimatedButton::OnButtonPressed, this, std::placeholders::_1));
	releaseEventHandler = button->AddEventHandler("release", std::bind(&AnimatedButton::OnButtonReleased, this, std::placeholders::_1));
	rollOutEventHandler = button->AddEventHandler("rollOut", std::bind(&AnimatedButton::OnButtonRollOut, this, std::placeholders::_1));

	animation = movie->SearchMovieInstance(animationName);
	animation->GotoAndStop(getIdleAnimationName().c_str());

	tap = false;
}

AnimatedButton::AnimatedButton() {
}

AnimatedButton::~AnimatedButton() {
	button->RemoveEventHandler("press", pressEventHandler);
	button->RemoveEventHandler("release", releaseEventHandler);
	button->RemoveEventHandler("rollOut", rollOutEventHandler);
}

void AnimatedButton::OnButtonPressed(Button* button) {
	tap = true;
	if (OnPress) {
		OnPress(button);
	}
	animation->GotoAndStop(getPressedAnimationName().c_str());
}

void AnimatedButton::OnButtonReleased(Button* button) {
	setCurrentNonPressedAnimation();

	if (OnRelease) {
		OnRelease(button);
	}
	if (tap) {
		OnButtonTap(button);
		tap = false;
	}
}

void AnimatedButton::OnButtonTap(Button* button) {
	if (OnTap) {
		OnTap(button);
	}
}

void AnimatedButton::OnButtonRollOut(Button* button) {
	if (OnRollOut) {
		OnRollOut(button);
	}
	tap = false;
	setCurrentNonPressedAnimation();
}

std::string AnimatedButton::getIdleAnimationName() {
	return "idle";
}

std::string AnimatedButton::getPressedAnimationName() {
	return "pressed";
}

std::string AnimatedButton::getDisabledAnimationName() {
	return "disabled";
}

void AnimatedButton::setCurrentNonPressedAnimation() {
	animation->GotoAndStop(button->enabled ? getIdleAnimationName() : getDisabledAnimationName());
}

void AnimatedButton::SetVisible(bool visible) {
	animation->SetVisible(visible);
}

void AnimatedButton::SetEnable(bool enable) {
	button->enabled = enable;
	setCurrentNonPressedAnimation();
}

void AnimatedButton::SetX(float x) {
	movie->SetX(x);
}

void AnimatedButton::SetY(float y) {
	movie->SetY(y);
}

float AnimatedButton::GetX() const {
	return movie->GetX();
}

float AnimatedButton::GetY() const {
	return movie->GetY();
}

Movie* AnimatedButton::GetMovie() const {
	return movie;
}

Movie* AnimatedButton::GetAnimation() const {
	return animation;
}

Button* AnimatedButton::GetButton() const {
	return button;
}

}
