#ifndef LWF_EVENTS_EXTENSION_H
#define LWF_EVENTS_EXTENSION_H

#include <functional>
#include <string>
#include "lwf_core.h"

namespace LWFNS {

void ListenEvents(LWF* lwf, char const* const eventType, function<void(std::string const& eventName)> listener);
void ListenEvents(shared_ptr<LWF> lwf, char const* const eventType, function<void(std::string const& eventName)> listener);

}

#endif // LWF_EVENTS_EXTENSION_H
