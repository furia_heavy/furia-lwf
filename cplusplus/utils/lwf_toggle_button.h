#ifndef LWF_TOGGLE_BUTTON_H
#define LWF_TOGGLE_BUTTON_H

#include <functional>
#include <string>
#include "lwf_animated_button.h"

namespace LWFNS {

class Movie;

class ToggleButton : public AnimatedButton {
public:
	ToggleButton(
		Movie* root,
		char const* const buttonName = "hitbox",
		char const* const animationName = "animation",
		bool on = true
	);
	ToggleButton();
	~ToggleButton();

	bool isOn();

protected:
	void OnButtonTap(Button*) override;

	std::string getIdleAnimationName() override;
	std::string getPressedAnimationName() override;
	std::string getDisabledAnimationName() override;

private:
	bool on;
};

}

#endif // LWF_TOGGLE_BUTTON_H
